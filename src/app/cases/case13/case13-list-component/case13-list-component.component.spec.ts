import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Case13ListComponentComponent } from './case13-list-component.component';

describe('Case13ListComponentComponent', () => {
  let component: Case13ListComponentComponent;
  let fixture: ComponentFixture<Case13ListComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Case13ListComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Case13ListComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
