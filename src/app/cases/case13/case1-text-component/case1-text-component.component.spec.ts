import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Case1TextComponentComponent } from './case1-text-component.component';

describe('Case1TextComponentComponent', () => {
  let component: Case1TextComponentComponent;
  let fixture: ComponentFixture<Case1TextComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Case1TextComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Case1TextComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
