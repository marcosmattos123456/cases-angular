import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-case4',
  templateUrl: './case4.component.html',
  styleUrls: ['./case4.component.scss']
})
export class Case4Component implements OnInit {

  constructor() { }

  ulItens = [
    'item 1',
    'item 2',
    'item 3',
    'item 4',
    'item 5',
  ]

  ngOnInit(): void {
  }

}
