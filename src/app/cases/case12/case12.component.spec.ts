import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Case12Component } from './case12.component';

describe('Case12Component', () => {
  let component: Case12Component;
  let fixture: ComponentFixture<Case12Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Case12Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Case12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
