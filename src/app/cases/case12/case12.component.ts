import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-case12',
  templateUrl: './case12.component.html',
  styleUrls: ['./case12.component.scss']
})
export class Case12Component implements OnInit {

  constructor(
    private fb: FormBuilder,
  ) { }

  form:FormGroup;
  boxList =  [
    {id: 1, check:false, label: 'Teste 1'},
    {id: 2, check:true, label: 'Teste 2'},
    {id: 3, check:false, label: 'Teste 3'},
    {id: 4, check:false, label: 'Teste 4'},
  ]

  clickHandler(id){
    this.boxList.map( item => {
      if(item.id == id) item.check = !item.check;
    })
    const selected = this.boxList.filter(item => {
      if(item.check) return item.id
    }).map(item => {
      return item.id
    }).join(',')

    this.form.get('seletedCheckBox').setValue(selected);
  }

  sendForm(){
    const data = this.form.getRawValue()
    this.form.reset();
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      seletedCheckBox: ['']
    })
  }

}
