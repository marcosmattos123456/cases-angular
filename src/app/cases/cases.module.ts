import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CasesComponent } from './cases.component';
import { CasesRoutingModule } from './cases-routing.module';
import { Case1Component } from './case1/case1.component';
import { Case2Component } from './case2/case2.component';
import { Case3Component } from './case3/case3.component';
import { Case3Service } from './case3/service/case3.service';
import { HttpClientModule } from '@angular/common/http';
import { Case4Component } from './case4/case4.component';
import { Case5Component } from './case5/case5.component';
import { Case6Component } from './case6/case6.component';
import { Case7Component } from './case7/case7.component';
import { BtnComponentComponent } from './case7/btn-component/btn-component.component';
import { Case7Service } from './case7/service/case7.service';
import { Case8Component } from './case8/case8.component';
import { BtnCase8Component } from './case8/btn-case8/btn-case8.component';
import { Case9Component } from './case9/case9.component';
import { Case10Component } from './case10/case10.component';
import { QuadradoCase10Component } from './case10/quadrado-case10/quadrado-case10.component';
import { Case11Component } from './case11/case11.component';
import { Case11Service } from './case11/service/case11-service.service';
import { Case12Component } from './case12/case12.component';
import { Case13Component } from './case13/case13.component';
import { Case13ListComponentComponent } from './case13/case13-list-component/case13-list-component.component';
import { Case1TextComponentComponent } from './case13/case1-text-component/case1-text-component.component';
import { Case14Component } from './case14/case14.component';
import { Case15Component } from './case15/case15.component';

@NgModule({
  declarations: [CasesComponent, Case1Component, Case2Component, Case3Component, Case4Component, Case5Component, Case6Component, Case7Component, BtnComponentComponent, Case8Component, BtnCase8Component, Case9Component, Case10Component, QuadradoCase10Component, Case11Component, Case12Component, Case13Component, Case13ListComponentComponent, Case1TextComponentComponent, Case14Component, Case15Component],
  imports: [
    CasesRoutingModule,
    CommonModule,
    HttpClientModule,
    DragDropModule,
    ReactiveFormsModule,
  ],
  providers: [
    Case3Service,
    Case7Service,
    Case11Service,
  ]
})
export class CasesModule { }
