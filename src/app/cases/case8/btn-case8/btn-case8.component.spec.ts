import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnCase8Component } from './btn-case8.component';

describe('BtnCase8Component', () => {
  let component: BtnCase8Component;
  let fixture: ComponentFixture<BtnCase8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnCase8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnCase8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
