import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-btn-case8',
  templateUrl: './btn-case8.component.html',
  styleUrls: ['./btn-case8.component.scss']
})
export class BtnCase8Component implements OnInit {

  constructor() { }
  @Input() text: string;
  @Input() fn: Function
  @Input() variavel:any = ''

  ngOnInit(): void {
  }

}
