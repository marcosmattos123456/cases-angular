import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-case8',
  templateUrl: './case8.component.html',
  styleUrls: ['./case8.component.scss']
})
export class Case8Component implements OnInit {

  constructor() { }
  showContent:boolean = false;
  
  toggleContent = () => {
    this.showContent = !this.showContent;
  }

  scrollTo = (top: boolean = true) => {
    top ? window.scrollTo(0,0) : window.scrollTo(0, document.body.scrollHeight)
  }

  scrollToElm(elm:string){
    document.getElementById(elm).scrollIntoView()
  }

  ngOnInit(): void {
  }

}
