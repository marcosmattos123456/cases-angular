import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuadradoCase10Component } from './quadrado-case10.component';

describe('QuadradoCase10Component', () => {
  let component: QuadradoCase10Component;
  let fixture: ComponentFixture<QuadradoCase10Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuadradoCase10Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuadradoCase10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
