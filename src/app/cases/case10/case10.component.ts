import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-case10',
  templateUrl: './case10.component.html',
  styleUrls: ['./case10.component.scss']
})
export class Case10Component implements OnInit {

  constructor() { }

  lista = []

  ngOnInit(): void {
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.lista, event.previousIndex, event.currentIndex);
  }


}
