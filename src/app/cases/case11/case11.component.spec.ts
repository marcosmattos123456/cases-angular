import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Case11Component } from './case11.component';

describe('Case11Component', () => {
  let component: Case11Component;
  let fixture: ComponentFixture<Case11Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Case11Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Case11Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
