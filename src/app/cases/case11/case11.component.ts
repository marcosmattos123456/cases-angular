import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Case11Service } from './service/case11-service.service';

@Component({
  selector: 'app-case11',
  templateUrl: './case11.component.html',
  styleUrls: ['./case11.component.scss']
})
export class Case11Component implements OnInit {

  constructor(
    private fb: FormBuilder,
    private formService: Case11Service,
  ) { }

  form:FormGroup;
  sucessMsg:string;

  sendForm(){
    const data = this.form.getRawValue()
    this.formService.sendForm(data).subscribe((res:any) => {
      this.sucessMsg = res.message;
      this.form.reset();
    })
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      nome: ['', Validators.required],
      idade: ['', Validators.required]
    })
  }

}
