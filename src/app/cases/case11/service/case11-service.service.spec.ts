import { TestBed } from '@angular/core/testing';

import { Case11ServiceService } from './case11-service.service';

describe('Case11ServiceService', () => {
  let service: Case11ServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Case11ServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
