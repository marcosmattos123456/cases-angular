import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable()
export class Case11Service {

  constructor() { }

  sendForm(data){
    console.log(data) //dados que vao pro servico
    //of do RxJs que retorma uma promisse pra simular
    return of({
      message: 'Formulario enviado com sucesso'
    })
  }

}
