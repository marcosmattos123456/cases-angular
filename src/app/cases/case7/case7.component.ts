import { Component, OnInit } from '@angular/core';
import { Case7Service } from './service/case7.service';

@Component({
  selector: 'app-case7',
  templateUrl: './case7.component.html',
  styleUrls: ['./case7.component.scss']
})
export class Case7Component implements OnInit {

  constructor(
    private case7Service:Case7Service
  ) { }
  options = [];
  currentText = 'Nenhum definido'

  receiverId(id){
    this.case7Service.getItem(id).subscribe((res:any) =>{
      this.currentText = res.text
    })
  }

  ngOnInit(): void {
    this.options = [
      {
        id:0,
        text: 'comp 0'
      },
      {
        id:1,
        text: 'comp 1'
      },
      {
        id:2,
        text: 'comp 2'
      },
      {
        id:3,
        text: 'comp 3'
      },
    ]
  }

}
