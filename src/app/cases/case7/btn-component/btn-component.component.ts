import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-btn-component',
  templateUrl: './btn-component.component.html',
  styleUrls: ['./btn-component.component.scss']
})
export class BtnComponentComponent implements OnInit {

  constructor() { }

  @Input() id = '';
  @Input() texto = 'sem texto';

  @Output() selectedId = new EventEmitter;

  clickHandler(id){
    this.selectedId.emit(id);
  }

  ngOnInit(): void {
  }

}
