import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class Case7Service {

  constructor(
    private http:HttpClient
  ) { }

  getItem(id){
    return this.http.get('./assets/json/case7.json').pipe(
      //map do rxJs, coisa linda
      map(
        //metodo find do ES6
        (data:any[]) => data.find(
          (item) =>{
            return item.id === id; 
          }
        )
      )
    )
    
  }

}
