import { TestBed } from '@angular/core/testing';

import { Case7Service } from './case7.service';

describe('Case7Service', () => {
  let service: Case7Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Case7Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
