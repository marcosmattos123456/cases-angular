import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-case2',
  templateUrl: './case2.component.html',
  styleUrls: ['./case2.component.scss']
})
export class Case2Component implements OnInit {

  constructor() { }

  //o certo aqui seria montar o form com o ReactiveForms
  //mas acho que n e o proposito do exemplo
  selectBoxItens = ['Tipo 1', 'Tipo 2', 'Tipo 3'];
  tableLines = []

  ngOnInit(): void {
    this.tableLines = [
      {id:0, tipo: 'Tipo 2', texto: 'um texto'},
      {id:1, tipo: 'Tipo 1', texto: 'outro texto'}
    ]
  }

  addLine(){
    this.tableLines.push({
      id: this.tableLines[this.tableLines.length-1].id + 1,
      tipo: '',
      texto: ''
    })
  }

  deleteLine(id:number){
    this.tableLines = this.tableLines.filter(item =>{
      return item.id !== id;
    })
  }
}
