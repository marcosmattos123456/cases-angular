import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-case15',
  templateUrl: './case15.component.html',
  styleUrls: ['./case15.component.scss']
})
export class Case15Component implements OnInit {

  constructor() { }

  valueList = [
    {value: 5, checked: false},
    {value: 10, checked: false},
    {value: 15, checked: false},
    {value: 50, checked: false},
    {value: 20, checked: false},
  ]

  clickHandler(value){
    this.valueList.map((item:any) =>{
      if(item.value === value) item.checked = !item.checked
    })
  }

  getSum(){
    let sum = 0
    this.valueList.filter((item:any)=>{
      if(item.checked) sum += item.value
    })
    return sum;
  }

  ngOnInit(): void {
  }

}
