import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-case1',
  templateUrl: './case1.component.html',
  styleUrls: ['./case1.component.scss']
})
export class Case1Component implements OnInit {
  
  displayText:string;
  showButton:boolean;

  constructor() { }

  clickHandler(){
    this.displayText = 'eu sou um bloco span, e para de clicar porque nao vou alterar mais nada.'
    this.showButton = false;
  }

  ngOnInit(): void {
    this.displayText = 'eu sou um bloco span'
    this.showButton = true;
  }

}
