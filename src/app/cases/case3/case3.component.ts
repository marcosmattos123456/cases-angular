import { Component, OnInit } from '@angular/core';
import { Case3Service } from './service/case3.service';

@Component({
  selector: 'app-case3',
  templateUrl: './case3.component.html',
  styleUrls: ['./case3.component.scss']
})
export class Case3Component implements OnInit {

  constructor(
    private case3Service: Case3Service
  ) { }

  texto1 = ''
  texto2 = ''
  texto3 = ''

  ngOnInit(): void {
  }

  clickHandler(){
    this.case3Service.getData().subscribe((res:any) =>{
      this.texto1 = res.texto1;
      this.texto2 = res.texto2;
      this.texto3 = res.texto3;
    })
  }

}
