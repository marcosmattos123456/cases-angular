import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class Case3Service {

  constructor(private http:HttpClient){
  }

  getData(){
    return this.http.get('./assets/json/case3.json');
  }

}
