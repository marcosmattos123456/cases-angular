import { TestBed } from '@angular/core/testing';

import { Case3Service } from './case3.service';

describe('Case3Service', () => {
  let service: Case3Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Case3Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
